# Cook with Cooper

This repository contains my favorite recipes, and files / config / CI for static site generation.

## Local development

### Requirements

* [Hugo](https://gohugo.io/)
* [pagefind](https://pagefind.app/)
* Python (for linting only)
* Docker (for linting only)
* Precommit

The Dockerfile for this repo has the dependencies set up.

### Submodules

This repo makes use of submodules. Make sure you clone the contents of the submodules:

```bash
git submodules init
git submodules pull
```

###

You can use hugo to run a basic webserver locally:

```bash
hugo serve
```

To rebuild the search index powering the search feature, use `pagefind`. This only needs to be ran occasionally, technically.

```bash
pagefind --site public
```

You can also use Docker to host the website locally, with the search function enabled

```bash
docker build . -t cookwithcooper:latest
docker run -v $(pwd):/src -p 1313:1313 -it cookwithcooper:latest /bin/sh -c "pagefind --site public; hugo serve --bind 0.0.0.0"
```

That's pretty much it!
