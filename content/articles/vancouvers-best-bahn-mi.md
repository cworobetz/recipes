---
date: "2025-02-27T22:14:00-08:00"
title: "Reviewing Every Single Bánh Mì in East Vancouver"
type: "article"
draft: false
params:
  toc: true
hidden: false
---

# In Which I Eat a Lot of Sandwiches

What makes great *bánh mì* great? Well, [there's a lot of things to consider](https://en.wikipedia.org/wiki/B%C3%A1nh_m%C3%AC), but I'm going to focus on what I consider important:

## The Components

### Bread

Bánh mì should be served on a french baguette, the fresher, the better. Still warm from the oven is preferable, but you're not going to get that at your average Vietnamese deli. The crust should be thick and crunchy, but not so much as to cut your mouth as you eat it. The inside should be soft and pillowy.

### Veg

A number of vegetables go into making bánh mì, some I'd consider essential, others less so

* Carrots and daikon. They should be pickled, and juilienned. The pickling should add a sweetness with the faintest hint of acidity from the vinegar
* Cucumber adds freshness and crunch
* Jalapeño adds some peppery spice, and is a welcome addition so long as it's not overpowering
* Fresh cilantro - and a lot of it

### Meats

Meats are going to have the most variation, since there's multiple different kinds you can order usually, and they'll all vary by restaurant. The heavy hitters are usually lemongrass pork & chicken, and some version of thinly slice roast meat, whether that's pork (most common) or chicken / beef. As with all components in this sandwich, you need to have not too much and not too little.

### Spreads

There's going to be two main spreads that I think every bánh mì should contain.

Mayo: This shouldn't come as a huge surprise, even for those not as familiar with bánh mì - it's a sandwich staple. The best thing a store can do to differentiate themselves is to make their own mayo. It's easy to prepare, and you can flavor it better. Try making your own mayo, and comparing it to Hellman's - you'll see what I mean.

Liver Pâté: Truly, a defining ingredient, and one that I'd love to see used a lot more in all sorts of foods. I ate this a lot growing up with my Danish family, so that explain why I have a certain affinity towards it. The liver pate adds a wonderful umami flavor that serves to elevate the rest of the ingredients. It's not the star of the show, but it certainly plays a very, very important role nonetheless. Liver pate is wonderful on its own, so it's hard to get this wrong.

The pate itself should be quite smotqh and cool, yet not too cold. This is for flavor, and to be easily spread on the sandwich. It should be salty, yet not overly so.

On the sandwich itself, we're really just looking for full edge-to-edge coverage and sufficient quantities. Too much, and you'll overpower that desirable freshness of the sandwich. Too little, and you won't get the flavor-permeation inherent to such a wonderful spread.
