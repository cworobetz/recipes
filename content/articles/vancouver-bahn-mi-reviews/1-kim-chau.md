---
date: "2025-02-28T12:26:00-08:00"
title: "Kim Chau"
type: "article"
draft: false
params:
  updated: "2025-03-02T20:47:00-08:00"
hidden: false
---

# Kim Chau

First on the list is [Kim Chau](https://maps.app.goo.gl/iqeBUpE3wbXJz21r6). They're been known for providing affordable bánh mì in Vancouver for many years. It's cash only, so be sure to hit up an ATM beforehand.

## The Components

### Bread

The bread does not appear to be freshly baked in-house. This is entirely normal, to be completely honest, but it's not worth of excessive praise. It has a decent crunch on the outside, yet it's betrayed by its decidedly not-fluffy interior. It's okay.

### Veg

Inside, we have the Vietnamese holy trinity of cilantro, pickled shredded carrot, and daikon. It's seasoned well, and there's actually the perfect amount inside - not overwhelming. The cilantro makes its presence known yet not overpowering-ly so. I asked for my bánh mì to be spicy, so there's a bit of jalapeno. It's fresh, crunchy, and not seedy enough to be too spicy. Perfect score on the veg.

### Spread

The mayo is mayo. No notes.

The pate is spread on the bottom half of the sandwich, and it blends in well with the other ingredients. I had to check it was even there. When I did taste for it though, it was delicious. I'm happy with it. Would I have preferred a little bit more of it? Absolutely.

### Meat

I got #4, the special roasted pork. They provide slightly less meat than I'd like, but it's still sufficient and that's just my preference anyways. The meat itself was well prepared and flavorful, tender and not chewy. I can't speak to the exact preparation method but it seems like they did a good job of it.

## Overall Impression

Kim Chau has a long history of providing affordable bánh mì in Vancouver's little Saigon, and even today that statement holds true. The individual ingredients taste great, with the exception of the bread, which was average at best.

### Recommendation

Worth eating at if you're in the area.
