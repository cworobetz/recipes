---
date: "2025-03-01T13:41:00-08:00"
title: "Obahnmi"
type: "article"
draft: false
hidden: false
---

# Obahnmi

Today, we went to Obahnmi's Kingsway location.

## The Components

### Bread

I found the baguette to be above average, which was a pleasant surprise. It was warm throughout when I picked it up, meaning they either baked it relatively recently or had reheated it at some point recently. The crust was crisp but not cutting, and the bread itself was pillowy. Not artisinal or anything, but they really did the best they could with what they had.

### Veg

Ah the veg. A bit disappointing to be honest, but --- just like all these reviews --- that's just my opinion.

I found the pickled carrots and daikon to be a little flimsy. They still had crunch as you bit through, but they were a bit soft and flimsy, as if they had been kept at too high a temperature, or kept in the brine too long. I'm far from an expert on pickling veg, but I believe they have room for improvement. All that being said, my girlfriend did express that she liked the daikon, so your mileage may vary here.

The cucumber was slippery and flimsy. It failed to add any freshness, disappointing for one of the key contributors to freshness.

The jalapeno was good. No complaints.

There wasn't enough cilantro for me. I couldn't really taste it in the sandwich.

Overall, it was acceptable for me but not good or great.

### Spread

The mayo was nothing special. It did its job.

The pate was flavorless. Even on isolated bites of bread with spread, I failed to pick up on its taste.

### Meat

I ordered the #7 "Deluxe Classic" today, which boasts 2 Viet ham & pate + pork belly. I will absolutely say they delivered here on the meat. They provided an ample amount, almost to the point of excess. This was the sandwiches saving grace for me.

## Overall Impression

An acceptable bánh mì. It lacks a certain balance in amounts of ingredients and freshness that I've found in other bánh mì.

### Recommendation

Good value for money, food wise, but only okay bánh mì. Worth a shot if you're in the area, you might like it more than I did.
