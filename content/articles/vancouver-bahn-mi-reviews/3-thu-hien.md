---
date: "2025-03-06T12:43:00-08:00"
title: "Thu Hien Deli & Sandwich Shop"
type: "article"
draft: false
hidden: false
---

# Thu Hien Deli & Sandwich Shop

Thu Hein, on Kingsway, looks disorganized from the inside but it contains some excellent Vietnamese ingredients. At the counter, you can order from one of ~4  bánh mì sandwich options. Their prices are low at $7 (cash or debit only)

## The Components

### Bread

On the plus side, the bread was warm. The crust was present and not overtly hard on the palette. It was not pillowy at all though, crisp all the way through.

### Veg

The veg was excellent. Well seasoned pickled veg, crisp cilantro and jalapeno - well done.

### Spread

The mayo, while clearly made in house, was lacking in quantity. I thought given the dryness of the bread, extra mayo would be appreciated.

There was no pate! A disappointment for sure, though I'm not sure if that's just because of the sandwich I ordered. Definitely something I'll need to check for in the future.

### Meat

I ordered the #13 "Shredded Pork Skin Sub" today. I was hoping for more of a crispy shredded skin situation, but that's not at all what I got. I can't deduct points for my own misunderstanding of the situation, but I wasn't a huge fan.

That said, the meat to veg ratio was in balance. Well done.

## Overall Impression

It's only okay. I think there's better options for bánh mì in the area, but the price is hard to beat.

### Recommendation

Check out other bánh mì in the area, unless you're really looking for the cheapest around.
