---
date: "2025-02-02T11:22:00-08:00"
title: "Sugar Cookies"
type: "recipe"
draft: false
hidden: false
---

# Sugar Cookies

**24 cookies**

---

## In the bowl of a stand mixer

* *198 g* softened butter
* *500 ml* sugar
* *375 ml* sugar

## In a tiny bowl

* *10 ml* vanilla

## On the counter

* *2* large eggs

## In a medium bowl

* *375 ml* all-purpose flour
* *2.5 ml* baking powder
* *2.5 ml* salt

## In a small bowl

* *125 ml* sugar

---

1. Preheat the oven to 350f
2. Beat the butter and sugar together until light and fluffy. Beat in vanilla, then eggs one at a time, until completely combined
3. On low speed, slowly mix in the flour mixture until just combined. Make sure there are no pockets of dry mix left
4. Using wet hands, roll 2 tablespoons of dough at a time into balls, then roll in sugar to coat. Place balls onto parchment lined baking sheets about 6 cm apart. Using the base of a glass that has been greased and sprinkled with sugar, press the cookies down to flatten. Sprinkle cookies with remaining sugar.
5. Bake the cookies, one sheet at a time, for 10 to 12 minutes, rotating the sheet halfway through
6. Let cookies cool on sheet for 10 minutes, then serve warm or transfer to wire rack to cool completely

## Notes

* Erin suggests adding cinnamon - she suggests *5 ml* into the flour mixture, and *2.5 ml* in the sugar rolling mix
* Source: America's Test Kitchen Family Cookbook
