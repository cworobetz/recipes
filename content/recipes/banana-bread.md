---
date: "2019-04-21T12:32:32-07:00"
title: "Banana Bread"
type: "recipe"
draft: false
---

# Banana Bread

**10 Servings**

---

* *565 g* (about 4) medium bananas, ripe but not blackened
* *115 g* plain greek yogurt
* *2* large eggs
* *15 ml* vanilla
* *280 g* all purpose flour
* *150 g* sugar
* *85 g* oat flour or whole wheat flour
* *5 ml* baking power
* *5 ml* baking soda
* *5 ml* kosher salt
* *5 ml* cinnamon
* *2.5 ml* teaspoon ground clove
* *2.5 ml* ground nutmeg
* *150 ml* coconut oil (creamy but firm)
* *170 g* toasted nuts (pecan, walnut) or chocolate chips

---

1. Line a 10.5" x 5.5" loaf pan, or two 9" x 5" loaf pans, with parchment paper. You can also do muffins.
2. Adjust oven rack to lower-middle position. Preheat to 350°F
3. Peel bananas, and mash together with Greek yogurt, eggs, and vanilla. Using a stick blender works well. If bananas are under ripe, let mixture sit for 30 minutes covered with plastic wrap
4. Combine flour, sugar, oat / whole wheat flour, baking powder, baking soda, salt, cinnamon, cloves, nutmeg and coconut oil in a large bowl. Mix together until coconut oil disappears into a mealy powder
5. Add banana mash and mix / fold just until the floury bits disappear
6. Fold in nuts or chocolate chips
7. Bake until well risen and golden brown. Internal temperature 206°F (Around 45 minutes for a small loaf, 70 minutes for a large loaf)
8. Store wrapped tightly in tin foil. It will keep for around 3 days at room temperature, or a week in the fridge. Can be frozen as well

## Notes

* Source: `https://www.seriouseats.com/recipes/2016/09/classic-banana-bread-recipe.html`
* Using flavored greek yogurt can work too if you don't have plain
* Have the parchment paper be protruding an inch or so on both of the long sides, to easily grab the loaf out of the pan. You don't need to have parchment paper lining the short walls
