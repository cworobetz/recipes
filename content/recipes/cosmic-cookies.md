---
date: "2019-10-12T15:22:31-07:00"
title: "Cosmic Cookies"
type: "recipe"
draft: false
---

# Cosmic Cookies

---

* *2 1/4 cups* quick oats
* *2 cups* spelt flour
* *1 cup* sugar
* *1 3/4 cups* vegan chocolate chips
* *1 1/4 cups* raisins
* *1 cup* sunflower seeds
* *3/4 cup* pumpkin seeds
* *1/2 cup* shredded coconut, unsweetened
* *1/4 cup* flax seeds
* *1 tbsp* cinnamon
* *2 tsp* salt
* *1 cup* dairy free milk, unsweetened
* *3/4 cup* neutral vegetable oil (e.g. canola)
* *1/4 cup* water
* *1/4 cup* molasses

---

1. Preheat oven to 350°F
2. Line 2 baking sheets with parchment paper
3. In a large bowl, combine all of the dry ingredients. Stir to combine
4. In a medium bowl, combine all of the wet ingredients
5. Add wet mixture to dry, mix until combined, being careful not to over mix
6. Use 1/3 cup measure to scoop cookie dough onto prepared baking sheets. Flatten very slightly, cookies should be quite tall so as to come out moist on the inside
7. Bake for 22-24 minutes, rotating the sheets halfway through

# Notes

* Source: `http://www.sweetlikecocoa.com/planet-organic-cosmic-cookies-v/`
* This recipe is vegan and dairy free
