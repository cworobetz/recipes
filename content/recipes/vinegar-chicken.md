---
date: "2020-11-30T01:53:07+00:00"
title: "Vinegar Chicken"
type: "recipe"
draft: false
---

# Vinegar Chicken

---

* salt
* pepper
* *2-3* chicken thighs
* *25 ml* garlic
* *45 ml* ketchup
* *75 ml* vinegar
* *45 ml* water

---

1. Cut the chicken thighs, skin side away, along the bones to expose the meat
2. Season chicken to taste with salt and pepper
3. Starting from a cold pan, place the chicken skin-side down on the pan and cook over high heat for a few minutes, until nicely browned. No fat needed
4. Cover the pan and cook on low - very low for 20-25 minutes, skin side down
5. Remove the cover and the chicken. The chicken should be very well browned. The juices should reduce down to fond in the pan
6. In the same pan, cook garlic for 30 seconds
7. Add vinegar and cook for 20-30 seconds to reduce acidity
8. Add water and ketchup (thickener), stir to combine
9. Pour pan sauce over chicken

## Notes

Source: `https://www.facebook.com/ChefJacquesPepin/videos/good-morning-here-is-a-chicken-with-a-vinegar-dish-from-my-hometown-of-lyon-in-f/767786963763917/`
