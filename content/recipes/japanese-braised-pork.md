---
date: "2023-12-16T11:00:34-08:00"
title: "Japanese Braised Pork"
type: "recipe"
draft: false
---

# Japanese Braised Pork Belly

---

* *2 1/2* cups dashi
* *1/4 cup* sake
* *45 ml* mirin
* *1/4 cup* sugar
* *1/4 cup* soy sauce
* *1* dried red chili pepper (optional)
* *1 lb* pork belly
* *9-10* pieces thinly sliced ginger (unpeeled; slice a 5 cm knob)
* *4* green onions (1 thinly sliced, 3 chopped into 2" pieces)
* shichimi togarashi (optional, to serve with)
* roasted white / black sesame seeds (optional, to serve with)

---

1. Pound 1 lb pork belly on both sides with a meat tenderizer
2. Press and mold the meat back into its original shape with your hands. Cut the pork belly into 5 cm pieces
3. Heat a heavy skillet (or pot) over medium-high heat, ungreased. When hot, add meat fat side down. Cook meat on all sides until nicely browned. A splatter screen is suggested
4. Transfer finished pieces to a paper lined tray
5. In a large pot, put the seared pork belly, rough chopped green onion, and half of the sliced ginger (saving the other half for the braising liquid). Next, add enough water to the pot to cover the meat
6. Bring to a boil, then reduce to a simmer. Cook, uncovered, for 2-3 hours. Turn pork occasionally. Add more water as needed. Longer cooking time yields more tender meat
7. Drain water and dry pork with paper towel to remove excess oil
8. To a large heavy pot, add cooked pork belly, dashi, sake, and mirin. Cook on medium heat
9. Add sugar, soy sauce, the rest of the ginger pieces, and (optional) dried red chili pepper
10. When it boils, reduce to low / medium low. Cook for one hour. Optionally, cover with an otoshibuta (or aluminum foil). I found it didn't reduce quick enough with the foil, so I opted to cook it uncovered
11. When the sauce has reduced *substantially*, to a nice glaze, toss the pork to ensure an even coating.
12. Serve over Japanese rice with thinly sliced green onion, shichimi togarashi, and toasted white + black sesame seeds

## Notes

* Source: `https://www.justonecookbook.com/braised-pork-belly-kakuni/`
