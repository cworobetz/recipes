---
date: "2020-01-04T13:57:21-08:00"
title: "0-template"
type: "recipe"
draft: true
hidden: true
---

# Title

**4 servings**

---

## In a small example bowl

* *10 ounces* flour
* *2.5 ml* baking soda
* *3* whole eggs

## In an example bowl

* *15 ml* oil

---

1. Make delicious food

## Notes

* Source: `https://recipemd.org/specification.html`
