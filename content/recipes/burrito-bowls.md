---
date: "2019-04-10T16:05:22-07:00"
title: "Burrito Bowls"
type: "recipe"
draft: false
---

# Burrito Bowls

---

* Cooked rice, brown or white (Preference)
* Avocado (Optional)
* Black beans (Optional)
* chopped tomato OR Salsa (Optional)
* Hot sauce of your choice (Optional)
* Burrito meat, e.g. carnitas, chicken breast (Optional)
* Corn kernels (Optional)
* Small dice red onion (optional)
* Chopped Cilantro (Optional)

---

1. Cook the rice according to direction
2. Prepare the meat of your choice
3. In an appropriately sized bowl, portion the rice in first, then stack rest of ingredients on the top in a fashion that pleases you

## Notes

* This recipe is more of a guideline, or a meta recipe that's composed of other single-ingredient producing recipes. Experiment with different rice types, meat types, veggie toppings and sauces.
* Some ideas:
   * Rice, guacamole, salsa, tortilla chips, and hot sauce for a quick snack
   * Rice pilaf instead of regular rice
   * Drizzle sour cream sauce on top
   * Use falafel for a vegan alternative to the meat
   * Augment the bowl by first placing down a tortilla
