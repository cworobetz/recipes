---
date: "2020-10-18T12:20:05-07:00"
title: "Buttermilk Cornbread"
type: "recipe"
draft: false
---

# Buttermilk Cornbread

---

* *318 g* yellow cornmeal
* *125 g* all-purpose flour
* *67 g* sugar
* *20 ml* baking powder
* *5 ml* salt
* *355 ml* buttermilk (at room temperature)
* *2* eggs (at room temperature, well beaten)
* *120 ml* unsalted butter (melted and cooled slightly)

---

1. Preheat oven to 400°F
2. Grease a 9x9 inch metal baking pan, or similar
3. In a large bowl, thoroughly combine the cornmeal, flour, baking powder, and salt.
4. In another bowl, mix together the buttermilk, eggs, and butter until just combined.
5. Stir the buttermilk mixture into the cornmeal mixture until just combined
6. Spread the batter into the prepared pan
7. Bake until the edges of the cornbread are just beginning to pull away from the sides of the pan and a knife inserted into the center comes out clean, 18 to 20 minutes.
8. Let stand in the pan for at least 5 minutes

# Notes

* Source: `https://www.yummly.com/recipe/Buttermilk-Corn-Bread-456535`
* When mixing the wet ingredients, if the ingredients are cold the fat in the butter may coagulate and the mixture may be clumpy. If this happens, you can gently heat the buttermilk + butter together in a saucepan until it melts together.
* If you mistakenly add hot ingredients to the eggs, the eggs will cook and you will need to discard and restart those ingredients.
