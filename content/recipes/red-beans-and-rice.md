---
date: "2025-02-08T12:29:00-08:00"
title: "Red Beans and Rice"
type: "recipe"
draft: false
hidden: false
---

# Red Beans and Rice

**10 servings**

---

# In a large bowl, 8 to 16 hours before cooking

* *450 g* dry kidney beans
* *1.5 l* cold water
* kosher salt

## In a medium bowl

* *450 g* cooked andouille sausage, sliced into 1 cm disks (see note)

## For cooking

* *15 ml* cooking fat (I used duck fat, oil works too)

## In a large bowl

* *1* large onion, small dice
* *4* ribs celery, small dice
* *1* green bell pepper, small dice
* salt

## In a small dish

* *4* cloves of garlic, minced

## In a second small dish

* *5 ml* (4 g) ground sage
* a generous amount of freshly ground black pepper
* *2.5 ml to 15 ml* cayenne pepper

## In the fridge

* 1 whole smoked ham hock (optional)

## In a third small dish

* *4 sprigs* fresh thyme (or *2 tsp* dry thyme)
* *3* bay leaves

## In a fourth small dish

* *15 ml* apple cider vinegar, or to taste

---

1. Soak the dry kidney beans for 8 to 16 hour, then drain them
2. In a large dutch oven, add the cooking fat and heat over medium high until shimmering. Add sausage in an even layer and let cook undisturbed until lightly browned on one side, about 3 minutes. Stir and continue to cook for around another 2 minutes
3. Add onion, carrot and celery. Cook, stirring, until vegetables have softened and are just starting to brown
4. Add garlic and cook until fragrant, 30 to 60 seconds
5. Add cayenne, sage, and black pepper. Cook until spices are fragrant, about 30 seconds
6. Add beans and enough water to cover by about 2 inches. Add thyme, bay leaves, and ham hock (if using). Bring to a boil and then reduce to a bare simmer. Cover and let cook until the beans have softened, 1.5 to 2.5 hours. Note: If the ham hock isn't completely covered, it's a good idea to rotate it occasionally
7. Remove the lid and continue to cook until the consistency is creamy, around 20 minutes more. Discard bay leaves and sprigs of thyme and serve over rice

## Notes

* If you cannot find andouille sausage, use the closest sausage you can find. I substituted raw hot italian sausage and it turned out great
* This recipe is easily adaptable. You have a large amount of freedom to adjust and make substitutions where you feel is approprirate
* This is an excellent meal prep recipe. It's cheap and it goes a long way
* Source: `https://www.seriouseats.com/new-orleans-style-red-beans-rice-recipe`
