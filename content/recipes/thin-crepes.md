---
date: "2019-12-02T17:07:35-08:00"
title: "Thin Crepes"
type: "recipe"
draft: false
---

# Thin Breakfast Crepes

---

* *150 g* flour
* *30 ml* sugar
* *2.5 ml* salt
* *2* eggs
* *375 ml* milk
* *2.5 ml* vanilla
* *15 ml* unsalted melted butter
* *30 ml* softened butter, for cooking

---

1. In a bowl, combine the flour, sugar and salt
2. Whisk in the eggs, half of the milk, and the vanilla
3. Gradually add the other half of the milk, stirring constantly
4. Whisk in melted butter
5. Heat a 23cm (9 inch) non-stick skillet over medium heat. When hot, brush with softened butter
6. Pour 50ml of batter in the skillet. Tilk the skillet to spread the batter evenly in a circle. Wait till completely cooked though, flipping if necessary
7. Place cooked crepes on a plate covered with tin foil for keeping. You can also use the oven on a low setting to keep them warm

# Notes

* Source: `https://www.ricardocuisine.com/en/recipes/5864-thin-breakfast-crepes`
* Topping ideas include maple syrup and butter, nutella and bananas, strawberry compote and whipped cream, or fresh assorted berries with syrup
* Icing sugar dispensed through a sieve can make a nice garnish over other toppings
