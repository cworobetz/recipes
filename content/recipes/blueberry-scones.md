---
date: "2019-12-06T21:28:54-08:00"
title: "Blueberry Scones"
type: "recipe"
draft: false
---

# Blueberry Scones

## Ingredients

* *240 g* flour
* *2.5 ml* salt
* *25 g* sugar
* *15 ml* baking powder
* *72 g* cold butter
* *200 g* blueberries (fresh or frozen)
* *325 ml* heavy cream (plus more for brushing the scones)
* *30 ml* lemon juice
* *60 g* confectioners' sugar
* *5 ml* unsalted butter
* *.5* lemon, zested finely (optional)

---

1. Preheat the oven to 400F
2. Sift together flour, salt, and baking powder
3. Using a pastry blender, cut in the cold butter. It should look like course crumbs
4. Fold the blueberries into the mixture
5. Make a well in the center and pour in the heavy cream
6. Work together to incorporate, do not overwork the dough
7. Press the dough out on a lightly floured surface into a rectangle shape, about 30cm x 8cm x 3cm
8. Cut into triangular shapes
9. Place uncooked scones onto a baking sheet with parchment paper.
10. Brush tops of scones with a little bit of cream
11. Bake scones for 15 to 20 minutes, until golden brown
12. Combine the lemon juice, confectioners' sugar, unsalted butter, and lemon zest in a microwave save bowl.
13. Microwave lemon glaze for 30 seconds, and whisk to remove any clumps
14. Apply lemon glaze to cooked scones when the scones have cooled slightly

# Notes

* Source: `https://www.foodnetwork.com/recipes/tyler-florence/blueberry-scones-with-lemon-glaze-recipe-1914780`
