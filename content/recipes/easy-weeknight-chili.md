---
date: "2024-11-25T18:32:00-08:00"
title: "Easy Weeknight Chili"
type: "recipe"
draft: false
hidden: false
---

# Easy Weeknight Chili

**8 servings**

---

## On hand

* *2 tbsp* vegetable oil
* pepper

## In a medium sized bowl

* *2* onions, medium dice
* *1* red bell pepper, medium dice
* *62 ml* (1/4 cup) chili powder
* *30 ml* (2 tbsp) cumin
* *5 ml* (1 tsp) cayenne pepper
* *5 ml* (1 tsp) kosher salt

## In a small dish

* *6 cloves* garlic, minced

## In another medium sized bowl

* *900 g* (2 lb) 85% lean ground beef

## In a large bowl

* *900 g* (2 16 oz) canned red kidney beans (drained and rinsed)
* *800 g* (28 oz) canned diced red tomatoes
* *800 g* (28 oz) canned tomato puree
* *5 ml* kosher salt

---

1. Heat vegetable oil over medium heat in a large dutch oven until shimmering. Add onion, pepper and spice mix and cook until the vegetables have softened, about 7 minutes. Stir in the garlic and cook until fragrant, 15 to 30 seconds more.
2. Increase the temperature to medium-high and add beef. Cook, breaking up the meat until no pink remains, about 8 minutes.
3. Stir in the large bowl of beans and tomatoes. Bring to a simmer, then reduce heat to maintain simmer. Cover and cook for 45 minutes.
4. Remove the lid and continue to simmer for about 45 minutes longer, until the beef is tender.
5. Season with salt and pepper to taste before serving

## Notes

* Inspired by America's Test Kitchen Family Cookbook
   * The spice amounts have been modified from the original
* Serve with shredded cheese (a sharp cheddar) broiled on top, sour cream, and / or sliced green onion
* Also goes great with [buttermilk cornbread]({{< ref "buttermilk-cornbread" >}})
