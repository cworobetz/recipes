---
date: "2025-02-01T11:45:00-08:00"
title: "Famichiki - Family Mart Fried Chicken"
type: "recipe"
draft: false
hidden: false
---

# Famichiki - Family Mart Fried Chicken

**4 cutlets**

---

## Prepare

* *280 g* (about 4) chicken thighs, pounded to an even thickness

## Marinade

* *1.5 g* salt
* *2.6 g* sugar
* *1 g* MSG
* *1 g* garlic powder
* *1.2 g* garlic paste
* *6 g* soy sauce
* *1.2 g* vinegar
* *0.9 g* baking soda
* *0.5 g* citric acid
* *52 g* water
* *34 g* flour

## Dry batter ingredients

* *6 g* salt
* *2 g* sugar
* *3 g* MSG
* *2 g* baking powder
* *0.5 g* powdered skim milk
* *1 g* powdered black pepper
* *0.1 g* garlic powder
* *0.5 g* paprika
* *1.1 g* panko

## In a separate bowl

* *92 g* flour
* *3 g* corn starch

## Wet batter ingredients

* *8 g* beaten egg
* *0.6 g* neutral vegetable oil
* *8.5 g* water

## For frying

* vegetable oil

---

1. Whisk together the marinade ingredients in a medium bowl until well combined. Add the pounded chicken thighs, and marinade for at least 3 hours at room temperature. (Food safety tip - ensure the chicken is very cold from the refrigerator. It should be okay to leave out for 3 hours if it starts from this  cold temperature)
2. In a mortar and pestle, crush the dry batter ingredients (particularly the panko and pepper). Mix well. In a medium bowl, combine with the flour and corn starch
3. Add the wet batter ingredients to the dry batter ingredients. Mix in *very* well with to produce tiny clumps in the batter. It should be mostly dry with wet clumps
4. Working one piece at a time, remove the chicken from the marinade and press into the batter to fully coat
5. Preheat the frying oil to about 350f. Add the chicken, the oil's temperature should drop. Maintain about 340f while frying for 90 seconds. Remove from oil and place onto wire rack placed onto a sheet pan.
6. Place the chicken on the wire rack + sheet pan combo into a 285f oven for 10 minutes to finish cooking through. Cooking in the oven replicates how Family Mart keeps their chicken hot throughout the day.
7. Enjoy!

## Notes

* Source: `https://www.youtube.com/watch?v=dET4bAfwcy0`
* When adding wet batter ingredients to the dry to produce clumps, whisk in *very* well to produce fine clumps. Famichiki is not like western fried chicken, the big knobbly bits are not present.
* Note for next time I make this - consider a slightly lower temperature for a much longer time in the oven. Chicken tasted excellent, but was a bit too flimsy
