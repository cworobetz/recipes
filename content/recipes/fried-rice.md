---
date: "2022-03-11T21:27:35-08:00"
title: "Fried Rice"
type: "recipe"
draft: false
---

# Fried Rice

**2 servings**

---

* *1* egg (whisked)
* *2* green onion OR garlic scapes
* *1* pinch sugar
* *15 ml* soy sauce, or to taste
* *10 ml* msg / chicken stock powder
* *15 ml* neutral oil
* *125 ml* assorted chopped veggies. frozen works well
* *250 ml* (dry) jasmine / sushi rice, day old, or **very** freshly cooked, preferably rinsed
* *2-3* chinese sausages, thinly sliced

---

1. Gather all ingredients
2. If using day old rice, break up the rice into individual grains
3. Preheat 2 tbsp oil in wok over max heat until smoking
4. Pour egg into the shallow oil pool. Allow to cook, poofing the egg up, for 5-10 seconds. Flip, then immediately slide back into original bowl
5. Preheat wok until smoking again. Cook Chinese sausage for 30-60 seconds, until light char occurs, or just cooked through. Return cooked sausage to egg bowl
6. Preheat wok until smoking again. Ensure there's a bit of oil in the pan. Cook the rice for 30-60 seconds, flipping and tossing to get the wok hei smokiness / slight char on each grain of rice. Return rice to bowl with egg / sausage
7. Preheat wok until smoking again. Cook veggies until slight char occurs.
8. Add all ingredients to the wok, including green onion, egg / rice / sausage, veggies, and sauces / spices. Cook 30-45 seconds until heated through

# Notes

* Source: Experience
* It's best if you can remove the flame guard over the burner, to produce a straight jet of flames onto your wok
* Always use max heat
* Garnish with more fresh green onion
* This is a smokey dish! Turn your hood fans to max, open doors, and consider turning on other fans to ensure good air flow
* Add frozen peas, shrimp, canned salmon as desired. It's good to use fresh, seasonal, and local ingredients
* This recipe is very adaptable. These steps use a good technique, but feel free to add / substitute ingredients
