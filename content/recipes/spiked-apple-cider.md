---
date: "2019-12-02T19:16:34-08:00"
title: "Spiked Apple Cider"
type: "recipe"
draft: false
---

# Spiked Apple Cider

---

* *950 ml* water
* *3* orange spice tea bags (or your preference)
* *125 ml* light brown sugar
* *500 ml* apple cider
* *375 ml* light rum
* *2+* cinnamon sticks
* *45 ml* butter

---

1. Boil water in a large saucepan
2. Remove from heat and toss in the tea bags. Cover and steep for 5 minutes
3. Remove tea bags
4. Stir in sugar, apple cider, rum, and two cinnamon sticks
5. Heat until just steaming - do not boil
6. Ladle hot cider into cups and garnish with a cinnamon stick

## Notes

* Source: `https://www.allrecipes.com/recipe/18942/hot-spiked-cider/`
