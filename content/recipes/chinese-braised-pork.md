---
date: "2023-07-29T15:40:53-07:00"
title: "Chinese Braised Pork"
type: "recipe"
draft: false
---

# Chinese Braised Pork

---

* *330 g* lean pork belly
* *30 ml* canola oil
* *15 ml* sugar (rock sugar is preferred)
* *45 ml* shaoxing wine
* *15 ml* light soy sauce
* *7.5 ml* dark soy sauce
* *500 ml* water

---

1. Mix the oil and sugar in a bowl
2. Mix the water, shaoxing wine, light soy sauce, and dark soy sauce in a bowl
3. Cut the pork belly into 2cm cubes
4. Bring a small pot of water to a boil
5. Blanch the pork belly in the water for 2 minutes. Remove and drain the pork and set aside
6. Melt the sugar oil in a wok over low heat
7. Raise the heat to medium high, add pork, and cook until lightly golden brown
8. Add the sauce mixture, reduce heat to a simmer. Cover for 45-60 minutes. Stir every 5-10 minutes
9. When the sauce has reduced to a thick coating, remove from heat and serve.

## Notes

* Source: `https://thewoksoflife.com/2014/04/shanghai-style-braised-pork-belly/`
* If the sauce doesn't reduce fast enough, you can remove some of the sauce, raise the heat, remove the cover, or some combination of those three
