---
date: "2020-01-04T13:57:21-08:00"
title: "Hoisin Sauce"
type: "recipe"
draft: false
---

# Hoisin Sauce

---

* *60 ml* soy sauce
* *30 ml* peanut butter
* *15 ml* honey
* *30 ml* rice vinegar
* *1 clove* garlic, grated
* black pepper, to taste
* *15 ml* miso paste

---

1. Combine all ingredients in a bowl and mix well

# Notes

* Source: `https://omnivorescookbook.com/homemade-hoisin-sauce`
* This will keep up to a month in the refrigerator
