---
date: "2018-02-24T17:02:54-08:00"
title: "Japanese Rice"
type: "recipe"
params:
  updated: "2025-02-25T09:24:00-08:00"
draft: false
---

# Japanese / Sushi Rice

---

* *500 ml* sticky rice
* *550 ml* water
* *15 ml* salt

---

1. Measure all of the ingredients into a rice cooker
2. Stir the rice to ensure the water and salt distribute evenly
3. Soak the rice for 30 minutes to 4 hours
4. Start the rice cooker and let it go until it turns off

## Notes

* Source: Experience
* This is not a recipe for making rice for sushi. That requires additional work, like seasoning the rice with a flavored vinegar solution. This recipe makes the rice you might receive as a side at a Japanese restaurant
* Exact cooking instructions depend on your rice cooker. For my rice cooker, it's best to immediately unplug the rice cooker after it's done - otherwise it may scorch on its "keep warm" setting
