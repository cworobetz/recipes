---
date: "2020-12-27T06:34:20+00:00"
title: "Pasta Aglio e Olio"
type: "recipe"
draft: false
---

# Pasta Aglio e Olio

---

* *225 g* pasta (fettucini, linguine, or pappardelle)
* *50 g* garlic (about half a bulb)
* *100 g* parley (about half a cup)
* *5 ml* red pepper flakes
* *30 ml* lemon juice (about half a lemon)
* *125 ml* olive oil
* salt
* pepper

---

1. Bring pot of water to a boil, and season water with a punch of salt. Try to only use a big of a pot as you need to fit the pasta
2. Finely mince the parsley
3. Slice the garlic into thin, flat pieces
4. Cook the pasta until al dente, or according to the package
5. Meanwhile, heat the olive oil over medium heat until shimmering in the pan
6. Add the garlic and cook at an even pace until just starting to brown on the edges. Stir constantly
7. Lower the heat to medium-low and add the red pepper flakes
8. Just before pasta is finished, reserve approximately 65ml of the pasta water to the side
9. Drain the pasta and add it and the reserved pasta water to the pan with the garlic
10. Add lemon juice and parsley
12. Season with salt and pepper

## Notes

* Source: `https://www.bingingwithbabish.com/recipes/2017/5/4/aglioeolio`
* This recipe would probably go great with some freshly grated parmesan over top
* If it's too watery at the end, you can continue to cook for 1-3 minutes until the pasta has absorbed more liquid
