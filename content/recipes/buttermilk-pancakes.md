---
date: "2017-08-12T17:53:39-07:00"
title: "Buttermilk Pancakes"
type: "recipe"
draft: false
---

# Buttermilk Pancakes

---

* *285 g* all purpose flour
* *5 ml* baking powder
* *2.5 ml* baking soda
* *5 ml* kosher salt
* *15 ml* sugar
* *2* large eggs, separated
* *375 ml* buttermilk
* *250 ml* sour cream
* *60 ml* unsalted butter, melted

---

1. Combine dry ingredients in a bowl
2. Whisk egg whites until stiff peak
3. Whisk egg yolks and buttermilk together
4. Slowly drizzle in melted butter to egg yolk buttermilk mixture while whisking
5. Carefully fold in the egg whites to egg yolk buttermilk mixture with rubber spatula
6. Pour the wet ingredients into the dry mix and fold until just combined
7. Using medium heat, oil a pan with butter and cook the pancakes until bubbles appear on the surface and it's golden brown

# Notes

* Source: `https://www.seriouseats.com/recipes/2010/06/light-and-fluffy-pancakes-recipe.html`
* You can pre-mix and store the dry ingredients for 3 months
* Buttermilk substitute can simply be 600ml of milk and 25ml lemon juice or vinegar
