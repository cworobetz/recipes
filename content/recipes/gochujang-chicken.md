---
date: "2024-08-30T16:37:41-07:00"
title: "Gochujang Chicken"
type: "recipe"
draft: false
---

# Gochugang Chicken

**6 Servings**

---

## For the sauce

* *1/3 cup* gochujang
* *60 ml* soy sauce
* *30 ml* mirin
* *30 ml* honey
* *3* cloves of garlic, grated
* *5 ml* sesame oil
* *5 ml* ginger, grated

## In a medium bowl

* *2 pounds* (~1 kg) boneless skinless chicken thighs, cut into bite-sized pieces

## On the side

* *15 ml* vegetable oil

## Garnish (optional)

* *10 ml* toasted sesame seeds (white and / or black)
* *3 whole* thinly bias-sliced green onions

---

1. Stir together 1/3 cup gochugang, 60 ml soy sauce, 30 ml mirin, 30 ml honey, 3 cloves of minced garlic, 5 ml sesame oil, and 5 ml grated ginger in a small bowl
2. Heat a wok over high heat. Add oil, then half of the chicken pieces. Stir fry until the chicken is no longer pink anywhere
3. Remove chicken from wok, and add the rest of the chicken and repeat. Remove from wok when done.
4. Wipe out any excess grease from the wok
5. Return wok to stove and heat over high heat. Stir sauce, and add sauce and chicken to wok
6. Cook until sauce is thickened and chicken is cooked through, about 3 minutes
7. Serve, and garnish with 10 ml toasted sesame seeds and 45 ml sliced green onions

## Notes

* Source: `https://www.allrecipes.com/sweet-and-spicy-gochujang-chicken-recipe-7497391`
* Serve over rice
* Pairs well with thinly sliced fresh cucumber
