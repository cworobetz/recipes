---
date: "2019-03-15T17:29:14-07:00"
title: "Salmon Fingers"
type: "recipe"
draft: false
---

# Salmon Fingers

---

* *125 ml* soy sauce
* *1* raw salmon fillet, skin removed
* *1* pinch white pepper
* *1* pinch garlic powder
* *62 ml* flour
* *62 ml* cornstarch
* *1 cup* canola oil
* green onion (optional)
* garlic chili paste (optional)

---

1. Cut the salmon fillet into rectangular shapes (fingers), roughly 6cm x 2cm x 1cm
2. Heat up canola oil in a cast iron pan for frying. You want about 5mm of oil in the pan, not enough to drown the salmon fingers
3. Pour soy sauce into shallow bowl or plate
4. Mix flour, white pepper, and garlic pepper together. Place into shallow bowl or plate
5. For each salmon finger, quickly coat all sides in the soy sauce, then transfer to flour to coat all sides. Set to side
6. When the pan has reached an appropriate temperature, fry the salmon fingers, flipping once, for about 30 seconds per side
7. Serve immediately, with green onion (optional) for garnish and garlic chili paste (optional) for some heat

## Notes

* Source: My dad
* Goes well with sticky rice (sticky-rice.md)
