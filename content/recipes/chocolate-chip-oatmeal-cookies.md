---
date: "2023-03-27T00:18:21-07:00"
title: "Chocolate Chip Oatmeal Cookies"
type: "recipe"
draft: false
---

# Chocolate Chip Oatmeal Cookies

---

* *125 grams* all purpose flour
* *1/2 teaspoon* ground cinnamon
* *1/2 teaspoon* baking soda
* *1/4 teaspoon* salt
* *150 grams* old-fashioned rolled oats
* *115 grams* softened unsalted butter
* *100 grams* light brown sugar
* *50 grams* sugar
* *1* large egg, at room temperature
* *1 teaspoon* vanilla extract
* *190 grams* semisweet chocolate chips

---

1. Whisk together flour, cinnamon, baking soda, and salt in a medium sized bowl
2. Stir in the oats
3. In a mixer with a paddle attachment, beat the butter, brown sugar, and sugar together for 1-2 minutes until well combined
4. Mix in the egg and vanilla extract, scraping bowl as needed
5. Gradually add the dry ingredients on a low speed until just combined, then mix in chocolate chips until just fully incorporated
6. Cover dough tightly with saran wrap and refrigerate at least 30 minutes
7. Preheat the oven to 350f
8. Using a small (1.5 tbsp) cookie scoop, portion out cookies on a baking tray lined with parchment paper
9. Bake at 350f for 10-13 minutes or until the tops of the cookies are set
10. Remove from the oven and cool on the baking sheet for 5-10 minutes, then transfer to a wire rack to cool completely

## Notes

* Source: `https://www.livewellbakeoften.com/soft-chewy-oatmeal-chocolate-chip-cookies/`
