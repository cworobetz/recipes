---
date: "2024-01-30T22:16:38-08:00"
title: "Japanese Hamburger Steak - Hambagu"
type: "recipe"
draft: false
---

# Japanese Hamburger Steak (Hambagu)

**4-6 servings**

---

* *1/2 onion* finely minced
* *15 ml* neutral oil
* *225 g* ground beef
* *110 g* ground pork
* *2.5 ml* salt
* *2.5 ml* black pepper
* *2.5 ml* nutmeg
* *80 ml* panko
* *30 ml* milk
* *1* large egg

## For Cooking the Hambagu

* *15 ml* neutral oil
* *45 ml* chicken or beef stock

## For the Pan Sauce

* *15 ml* butter
* *45 ml* ketchup (to taste)
* *45 ml* tonkatsu sauce
* *45 ml* water

---

### To Make the Pattys

1. Saute the minced onion over medium heat with the oil until tender and almost translucent. Transfer to a large bowl and let cool
2. Once cooled, add beef, pork, salt, pepper, nutmeg, panko, milk, and egg (you can also use 100% ground beef, but it will not be as juicy and tender)
3. Knead together with your hands until it's sticky and pale
4. Divide into 4 (4oz / 113g) to 6 portions. Form into a rounded oval patty. Toss between your hands a few times to release any air inside the mixture, to reduce cracks while cooking
5. Place patties on a tray and placed tray, covered, in fridge for at least 30 minutes so that the meat combines and the fat solidifies

### Cooking the Hambagu

1. Heat a large pan over medium and add oil. Gently place patties, and indent the center of each patty with two fingers as the center will rise with heat
2. Cook until well browned on bottom, about 3 minutes. Flip and cook 3 more minutes
3. Add stock, reduce heat to med-low, cover pan with lid. Cook 5-7 minutes to ensure patties cook through
4. Uncover and check doneness. If clear juice comes out when stabbed, it's done.
5. Increase heat to medium and wait for liquid to almost evaporate. Do not wash the pan, as you will use the juices

### Making the Pan Sauce

1. In the pan, add butter, ketchup, tonkatsu sauce, stock, and water
2. Whisk well together and bring sauce to a simmer over medium. Let sauce reduce until it coats the back of a spoon
3. Remove from heat when done and drizzle over the hambagu

## Notes

* Source: `https://www.justonecookbook.com/hamburger-steak-hambagu/`
* This stores, cooked, in the fridge for up to 3 days or a month in the freezer
* For the tonkatsu sauce, you can substitute Worcestershire sauce and sugar in a pinch
