---
date: "2022-06-27T00:33:21-07:00"
title: "Pork Katsudon"
type: "recipe"
draft: false
---

# Pork Katsudon

---

* *2* medium-sized pork chops
* salt and pepper
* *3* eggs
* *75 ml* all-purpose flour
* *1 cup* panko
* oil (for frying)
* *125 ml* dashi stock or chicken stock (I use chicken stock usually)
* *15 ml* soy sauce
* *10 ml* mirin
* *10 ml* sugar
* *1* medium onion
* *180 ml* raw jasmine rice, steamed
* *1* scallion (optional)

---

1. Pound out the pork until 1cm thick. Season with salt on both sides, and place into the refrigerator uncovered for an hour
2. Combine the stock, soy sauce, mirin, and sugar until completely mixed together into a thin sauce
3. Slice the onion into thin semi-circles
4. Beat two of the eggs into a bowl, and put into fridge for later
5. Season the pork with pepper on both sides
6. Beat one egg into a shallow dish. Pour flour into another shallow dish. Add panko to yet another shallow dish
7. Preheat the frying oil in a pan. There should be about 1cm of oil
8. Begin slowly cooking the onions in a small pan with oil. Add salt to the onions at the beginning
9. Flour the pork pieces, then dip them into the egg wash, then press them into the panko until completely coated
10. When the oil is hot enough to fry, cook the pork, turning often, until golden brown and cooked through
11. Place pork on paper towel to soak up excess grease
12. When the onions are soft and beginning to brown, add the thin sauce and wait for the sauce to simmer
13. Slice fried pork cutlets into thin strips and place on top of onions
14. Pour two beaten eggs on top of sliced pork and sauce in the pan.
15. Cover the pan and wait for the eggs to cook. They should be very soft, even runny if desired
16. Place cooked rice into serving dish(es) and slide pork katsudon from the pan over top of the serving dish
17. Garnish with green onion

## Notes

* Source: `https://thewoksoflife.com/katsudon/`
* This recipe is interesting for its intentional putting of the fried pork cutlet into the sauce. It makes the breading soggy, yet flavorful
