---
date: "2017-07-31T22:29:04-07:00"
title: "Chicken-Fried Chicken"
type: "recipe"
draft: false
---

# Chicken-Fried Chicken

**4 servings**

---

## In a small bowl

* *30 ml* paprika
* *30 ml* black pepper
* *10 ml* garlic powder
* *10 ml* died oregano
* *2.5 ml* cayenne pepper

## Prepared and put aside

* *2* 8 oz chicken breasts, cut and pounded into 4 cutlets

## In a medium bowl

* *250 ml* buttermilk
* *1* large egg
* *15 ml* kosher salt

## In a large bowl

* *375 ml* all-purpose flour
* *10 ml* kosher salt
* *125 ml* cornstarch
* *5 ml* baking power

## In a frying vessel

* *1 l* frying oil

---

1. Combine paprika, black pepper, garlic powder, oregano, and cayenne pepper in a small bowl and mix thoroughly
2. Pound out chicken until about 1/4" thick
3. Whisk buttermilk, egg, 15ml salt, and 30ml of the spice mixture in a large bowl
4. Add chicken to wet mixture and turn to coat. Transfer bowl contents to a large freezer bag and refrigerate for at least 4 hours and up to overnight, flipping the bag occasionally to redistribute the contents and coat the chicken evenly
5. Whisk together the flour, cornstarch, baking powder, 2 teaspoons salt, and the remaining spice mixture in a large bowl.
6. Add 3 tablespoons of the marinade from the zipper-lock bag and work it into the flour with your fingertips. Remove the chicken from the bag, allowing the excess buttermilk to drip off.
7. Drop the chicken into the flour mixture and toss and flip until thoroughly coated, pressing with your hand to get the flour to adhere in a thick layer.
8. Shake the chicken over the bowl to remove excess flour, then transfer to a large plate.
9. Adjust an oven rack to the middle position and preheat the oven to 175°F.
10. Heat the shortening or oil to 425°F in a wok or 12-inch cast iron skillet over medium-high heat, about 6 minutes. Adjust the heat as necessary to maintain the temperature, being careful not to let the fat get any hotter.
11. Carefully lower 2 pieces of chicken into the pan. Adjust the heat to maintain the temperature at 325°F for the duration of cooking. Fry the chicken pieces without moving them for 2 minutes. Carefully agitate the chicken with a wire-mesh spider or tongs, making sure not to knock off any breading, and cook until the bottom is a deep golden brown, about 3 minutes. Carefully flip the chicken and continue to cook until the second side is golden brown, about 2 minutes longer.
12. Transfer the chicken to a paper towel–lined plate to drain for 30 seconds, flipping once, then transfer to a wire rack set on a rimmed baking sheet and transfer to the oven to keep warm. Repeat with the remaining 2 pieces of chicken.

## Notes

* Source: `www.seriouseats.com/recipes/2015/07/chicken-fried-chicken-with-cream-gravy-recipe.html`
