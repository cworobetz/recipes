---
date: "2024-11-19T18:55:00-08:00"
title: "Basic Teriyaki Sauce"
type: "recipe"
draft: false
hidden: false
---

# Basic Teriyaki Sauce

**8 servings**

---

## In a small saucepan

* *1 cup* (275 g) japanese soy sauce
* *1 cup* (275 g) mirin
* *1/2 cup* (118 g) sake
* *1/2 cup packed* (103 g) dark brown sugar

## In a small bowl

* *5 ml* grated ginger

---

1. Mix soy sauce, sugar, mirin, and sake together in a small saucepan. Bring mixture to a boil over medium-high heat, reduce to a simmer, and cook until thickened enough to coat a spoon, about 20 minutes. Remove from heat, let cool, and use immediately or store in an airtight container in the refrigerator.

## Notes

* Source: `https://www.seriouseats.com/teriyaki-sauce-how-to-make-at-home-recipe`
