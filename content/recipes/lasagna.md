---
date: "2024-01-23T21:59:35-08:00"
title: "Lasagna"
type: "recipe"
draft: false
---

# Lasagna

---

* 425g ricotta cheese
* 70g (325ml) grated parmesan cheese
* 125ml minced basil
* 1 large egg, beaten
* salt
* pepper
* olive oil
* 1 onion, minced
* 6 garlic cloves, minced
* 825ml (large can or two small) diced fire-roasted tomatoes
* 825ml (large can or two small) crushed tomatoes
* 2.5ml oregano
* 2.5ml red pepper flakes
* 5ml onion powder
* 5ml garlic powder
* 225g no-boil lasagna noodles
* 450g mozzarella
* 225g minced pork (lower fat if possible)
* 225g minced beef (lower fat if possible)
* 2 zucchinis, cut into small quarters

## Directions

### Sauce

1. Heat oil in a large pot / saucepan over medium until shimmering. Add onion and zucchini and 5ml salt and cook until onions are softened, about 5 minutes
2. Add garlic and cook until fragrant, about 30 seconds
3. Add meat and cook until almost all of the pink is gone
4. Stir in tomatoes with juice, oregano, onion powder, garlic powder, red pepper flakes, and pepper (to taste, season the top at least). Simmer until thicker, 20-25 minutes. Season with salt and pepper to taste

### Ricotta Mixture

1. Mix ricotta, 250ml parmesan, basil, egg, salt, and pepper until well combined

### Assembly

1. Spread 75ml tomato sauce on the bottom of a 9x13" casserole dish.
2. Layer noodles until it covers most* (75% or so) of the pan. Spread ~50ml of ricotta mixture out even over noodles (and sauce as possible). Sprinkle even with 250ml of mozzarella. Spoon 375ml of sauce evenly over the cheese. Repeat 2x
3. For the final layer, place noodles as before. Spread remaining ~325ml of sauce over the top. Sprinkle with remaining ~250ml mozzarella and then the remaining ~65ml parmesan.
4. Cover the lasagna with foil, and bake for 15 minutes
5. Remove the foil, and bake for 25 minutes, until golden brown and heated to at least 165f internally (it will continue to raise in temperature outside the oven)
6. Let sit for 10 minutes before serving

## Notes

* Source: The America's Test Kitchen Family Kitchen Cookbook Revised Edition
* This will last (covered) for 4-5 days in the fridge, and several months in the freezer, uncooked, and for a shorter period cooked
* If baking from refrigerated, wait at an hour after removing it from the fridge before baking to allow the lasagna to warm through more evenly while baking
* This recipe is approximate in many dimensions (especially cheese and seasonings), so don't worry too much if it's not exact
