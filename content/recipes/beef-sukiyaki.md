---
date: "2023-12-16T10:36:44-08:00"
title: "Beef Sukiyaki"
type: "recipe"
draft: false
---

# Beef Sukiyaki

---

* *1 1/2 cups* dashi stock
* *3/4 cup* soy sauce
* *3/4 cup* mirin
* *1/4 cup* sugar
* *8 oz* shirataki noodles
* *45 ml* cooking oil (I used rice bran oil)
* *1 lb* beef top sirloin, thinly sliced
* *1* medium onion, thinly sliced
* *2* medium carrots, thinly sliced on a bias
* *2* red bell peppers, cut into medium cubes
* *8 oz* napa cabbage, chopped into 1" pieces
* *5* green onions, cut into 2" pieces
* *14 oz* firm tofu, cut into cubes

---

1. Prepare all of the ingredients
2. Combine dashi, soy sauce, mirin, and sugar in a bowl
3. Soak the noodles in boiling water for 1 minute. Drain, rinse under cold water, and drain again
4. Heat 30ml of oil in a large pot over medium heat. Add beef and cook and stir until no longer pink, about 2-3 minutes. If there's excess liquid, soak it up using paper towels. Transfer cooked beef to a plate
5. Add remaining 15ml of oil to pot and heat over medium heat. Add onion, carrots, bell peppers, and napa cabbage; cook and stir until softened, about 4-6 minutes.
6. Add sauce mixture, noddles, beef, tofu, and green onions; bring to a simmer
7. Remove from heat and ladle into bowls

# Notes

* Source: `https://www.allrecipes.com/recipe/231781/beef-sukiyaki/`
* Be careful about simmering the soup for too long. The vegetables will wilt
* This recipe includes variations from the original, to make it to Erin's taste while she's sick. It includes bell peppers for additional vitamin C, napa cabbage (which I had extra of, and was delicious), and it removes the celery and mushroom
* This recipe requires quite a large pot. It still tastes great when cooled and reheated the next day
* I found shirataki noodles in the refrigerated noodle section of T&T supermarket. They were in a bag filled with water
* If possible, find beef with a lower fat content. I chose a cheaper cut of beef, and the amount of fat made it render out too much and had to be soaked up
