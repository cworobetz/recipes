---
date: "2023-11-19T11:24:55-08:00"
title: "Waffles"
type: "recipe"
draft: false
---

# Waffles

**4 Servings**

---

* *10 ounces* flour
* *2.5 ml* baking soda
* *5 ml* baking powder
* *7.5 ml* kosher salt
* *30 ml* sugar
* *3* whole eggs
* *500 ml* buttermilk
* *45 ml* melted butter

---

1. Mix all of the dry ingredients together
2. Mix the wet ingredients together separately
3. Add the wet ingredients to the dry ingredients,
   and mix until there's no pockets of flour remaining
4. Add batter to preheated waffle iron and cook following the waffle iron's instructions

## Notes

* Source: `https://www.seriouseats.com/bacon-cheese-scallion-waffle`
* This recipe should make 4 standard sized waffles
* To half this recipe, just use one egg
* This is a great base recipe. You can fold in other ingredients afterwards: 6oz bacon (replacing 15ml of butter for bacon fat), 6oz grated cheddar cheese, and 6 scallions makes a great savory waffle
