---
date: "2019-10-27T15:45:55-07:00"
title: "Basic Chicken Breast"
type: "recipe"
draft: false
---

# Basic Chicken Breast

**2 Servings**

---

* *2 whole* chicken breasts
* *30 ml* cooking fat (olive oil and butter work well)

---

1. Heat fat in a pan over medium-high heat until just smoking
2. Sear the chicken breast on one side for 45-90 seconds, until it has a nice color
3. Flip the chicken and continue to sear for 45-60 seconds, until it has a nice color
4. Cover the pan and reduce heat to medium-low / low for 10 minutes. Don't peek. You want it to barely simmer the liquid.
5. After 10 minutes, turn off the heat and let cook an additional 10 minutes. Don't peek.
6. Rest the chicken breast for 2 minutes after removing from heat

## Notes

* Source: A Reddit user
* At the very least, salt and pepper your chicken prior to cooking
* Marinades are an excellent idea as well to add additional flavor
