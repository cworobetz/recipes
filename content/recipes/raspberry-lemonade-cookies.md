---
date: "2024-05-06T12:05:40-07:00"
title: "Raspberry Lemonade Cookies"
type: "recipe"
draft: false
---

# Raspberry Lemonade Cookies

**12 Servings**

---

## In a large mixing bowl or stand mixer bowl**

* *113 g* unsalted butter, softened
* *110 g* light brown sugar
* *50 g* granulated sugar
* *10 ml* freshly grated lemon zest

## In a small bowl

* *1* large egg
* *5 ml* lemon juice
* *5 ml* pure vanilla extract

## In a medium bowl

* *215 g* all-purpose flour
* *2.5 ml* salt
* *2.5 ml* baking powder
* *1.25 ml* baking soda

## In another medium bowl

* *85 g* frozen raspberries, crumbled

---

1. Cream the butter, sugars, and lemon zest well
2. Mix in the egg, lemon juice and vanilla until just well combined
3. In a medium bowl, mix flour, baking soda, baking powder and salt together well. Add the dry ingredients to the wet ingredients and mix until mostly incorporated
4. Crumble up the frozen raspberries (a pastry cutter can speed up the process) and fold them into the dough gently
5. Scoop cookies, and place into the fridge or freezer
6. Heat the oven to 350f
7. Bake 12-14 minutes until golden brown all over. Don't under cook the cookies. Let cool a couple of minutes then transfer to a wire rack to finish cooling

## Notes

* Source: `https://scientificallysweet.com/raspberry-lemonade-cookies/`
* Ensure the raspberries are fully frozen while crumbling to ensure you don't crush all the juices out
