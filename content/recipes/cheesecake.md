---
date: "2019-02-03T19:42:08-08:00"
title: "Cheesecake"
type: "recipe"
draft: false
---

# Cheesecake

**8 servings**

---

## Crust

* *1 3/4 cups* graham cracker crumbs
* *2/4 teaspoon* ground cinnamon
* *1/3 cup* unsalted butter, melted

## Filling

* *32 oz* cream cheese, left out 8 hours
* *1 1/4 cups* sugar
* *1/2 cup* sour cream
* *10 ml* vanilla extract
* *5* large eggs

## Topping

* *1/2 cup* sour cream
* *2* teaspoons sugar

---

1. Preheat oven to 475f. Place a large pan filled with 1/2 inch water in oven.
2. Mix graham cracker crumbs and cinnamon, add melted butter
3. Press crust onto bottom and 2/3 of the way up a 9-inch spring form pan lined with parchment. Wrap a large piece of foil around bottom of pan. Freeze until filling is prepared
4. Make the filling: Beat together cream cheese, sugar, sour cream, and vanilla. Blend until smooth and creamy. Scrape down sides of bowl
5. Whisk eggs in a separate bowl, add to cream cheese mixture. Blend until just incorporated
6. Remove crust from freezer and pour in filling. Carefully plate cheesecake into preheated water bath.
7. Bake for 12 minutes, turn oven to 350f and bake 50-60 minutes longer, until top of cheesecake turns golden.
8. Cool on wire rack
9. When completely cool, mix 1/2 cup sour cream and 2 teaspoon sugar. Spread on top of cheesecake. Refrigerate

# Notes

* Source: `https://www.myrecipes.com/recipe/cheesecake-factory-original-cheesecake`
* Remove all ingredients from fridge ahead of time. The closer to room temperature, the less cracking you'll get.
