---
date: "2022-12-06T15:23:02-08:00"
draft: false
title: "Bacon and Leek Quiche"
type: "recipe"
---

# Bacon and Leek Quiche

**8 Servings**

---

* *100 g* grated cheddar cheese
* *250 g* plain flour
* *110 g* cubed cold butter
* *1 pinch* salt
* *30 ml* poppy seeds
* *1 knob* butter
* *6 rashers* smoked bacon
* *1* large leek
* *6* eggs
* *1 bunch* fresh chopped parsley
* *60 ml* water
* *100 g* soft goats cheese
* *500 ml* double cream

---

1. Blitz flour, salt, poppyseeds and butter in a food processor until like breadcrumbs
2. Gradually add water and mix until a dough is just formed
3. Preheat oven to 392F (200C)
4. Roll out the pastry until it is as thin as a loony. Ease into a 24cm pie / tart shell, push into corners, and trim away the edges
5. Place wax paper over the pie shell and fill with dry beans or rice. Rest in fridge for 30 minutes. Bake the pastry for 10 minutes, then remove the wax paper and rice / beans. Cook for 5 minutes more
6. Turn the oven down to 320F (160C)
7. Chop bacon into small pieces. Half and slice the leek thinly, and wash away all the grit between the leaves
8. Fry the bacon and leek in a pan with the knob of butter and plenty of black pepper. Toss in the parsley and put the mixture into a sieve to drain any excessive liquid
9. Spoon the filling into the pie shell and scatter with cheddar cheese
10. Whisk the eggs and double cream in a bowl and season with salt and pepper. Pour the egg mixture over the leek and bacon until it almost reaches the top of the pie shell
11. Bake the quiche for 45 minutes until the egg has just a slight wobble and is golden all over

## Notes

* Source: `https://sortedfood.com/recipe/baconleekquiche`
* A rasher of bacon is just a slice of bacon, for those of us outside the UK :)
