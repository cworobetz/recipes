---
date: "2024-11-24T11:53:00-08:00"
title: "A Better Quick Chicken Stock"
type: "recipe"
draft: false
hidden: false
---

# A Better Quick Chicken Stock

**1 cup**

---

## Ingredients

* *50 ml* cold water
* *200 ml* near-boiling water
* *7.5 ml* powdered gelatine
* *5 ml* better than bouillion chicken stock concentrate

---

1. Sprinkle gelatine over the cold water. Set aside and let bloom for 5 minutes
2. Combine chicken stock concentrate with boiling water
3. After 5 minutes have passed, combine the chicken stock with the gelatin water

## Notes

* Source: Serious Eats
* This improves the mouth feel of the chicken stock
