FROM docker.io/hugomods/hugo:latest

RUN wget -O pagefind.tgz https://github.com/CloudCannon/pagefind/releases/download/v1.2.0/pagefind-v1.2.0-x86_64-unknown-linux-musl.tar.gz
RUN tar -xzf pagefind.tgz && rm pagefind.tgz
RUN mv pagefind /bin/