all
exclude_rule "MD013" # I like long line length, if required
exclude_rule "MD036" # Fires on the recipes.md servings section
exclude_rule "MD041" # First line in file is Hugo frontmatter
rule 'MD029', :style => 'ordered'